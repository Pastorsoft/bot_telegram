import time ##Importamos el modulo tiempo
import os ##Importamos el modulo del sistema
import RPi.GPIO as GPIO ##Importamos los GPIOS
import telegram ##importamos modulo de Telegram anteriormente bajado
from telegram.ext import *

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(21, GPIO.IN)

GPIO.output(27, True)

mi_bot = telegram.Bot(token="459472203:AAHClTMdjO-1A7LDfm_9APkJ7EsD51B05Uk")
mi_bot_update = Updater(mi_bot.token)

##############################

# Funcion para realizar llamadas del sistema


def llamadaSistema(entrada):
    salida = ""  # Creamos variable vacia
    f = os.popen(entrada)  # Llamada al sistema
    for i in f.readlines():  # Leemos caracter a caracter sobre la linea devuelta por la llamada al sistema
        salida += i  # Insertamos cada uno de los caracteres en nuestra variable
    salida = salida[:-1]  # Truncamos el caracter fin de linea '\n'

    return salida  # Devolvemos la respuesta al comando ejecutado

##############################

def encendido(bot, update):
	GPIO.output(17, True) # Aqui hacemos peticion para lectura de un archivo
	update.message.reply_text("Encendido")
	var2 = llamadaSistema('cat /sys/class/gpio/gpio21/value')
	if GPIO.input(21):
		GPIO.output(27, False)


def apagado(bot, update):
    GPIO.output(17, False)  # Aqui hacemos peticion para lectura de un archivo
    update.message.reply_text("Apagado")
    if GPIO.input(21):
        pass
    else:
        GPIO.output(27, True)
def temporizado(bot, update):
	encendido(bot, update)
	update.message.reply_text("Empieza la cuenta atras...")
	while(True):
		
		Hora_actual = time.strftime("%H:%M:%S")
		GPIO.output(22, True)
		time.sleep(0.5)
		GPIO.output(22, False)
		time.sleep(0.5)
		if():
			apagado(bot, update)
			break
		else:
			if(Hora_actual == "18:44:00"):
				apagado(bot, update)
				break
			

mi_bot_update.dispatcher.add_handler(CommandHandler('temporizado', temporizado))
mi_bot_update.dispatcher.add_handler(CommandHandler('encendido', encendido))
mi_bot_update.dispatcher.add_handler(CommandHandler('apagado', apagado))


mi_bot_update.start_polling()
mi_bot_update.idle()
