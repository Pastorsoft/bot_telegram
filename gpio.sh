#!/bin/bash

###exportamos los puertos necesarios##

echo 17 > /sys/class/gpio/export
echo 27 > /sys/class/gpio/export
echo 21 > /sys/class/gpio/export
echo 22 > /sys/class/gpio/export

###Los ponemos como salida ####

echo out > /sys/class/gpio/gpio17/direction
echo out > /sys/class/gpio/gpio27/direction
echo out > /sys/class/gpio/gpio22/direction

##Este lo colocamos como entrada ##

echo in > /sys/class/gpio/gpio21/direction

##encendemos el aparato
echo 1 > /sys/class/gpio/gpio27/value

function parpadeo {
        echo 1 > /sys/class/gpio/gpio22/value
        sleep 0.5
        echo 0 > /sys/class/gpio/gpio22/value
	sleep 0.5
}

function encender {
	echo 1 > /sys/class/gpio/gpio17/value
	echo "Encendido a la hora " $actual
	var2= cat /sys/class/gpio/gpio21/value
	if [ $var2=='1' ]; then ##compara si esta encendido led verde y apaga el rojo
    		echo 0 > /sys/class/gpio/gpio27/value
  	fi
}

function apagar {
  	echo 0 > /sys/class/gpio/gpio17/value
  	echo "Apagado a la hora " $actual
	if [ $var2=='0' ]; then ## mira si esta apagado el verde y enciende el rojo
    		echo 1 > /sys/class/gpio/gpio27/value
  	fi
}

while [ true ]; do
read var1
actual=`date +"%H:%M:%S"` ##aqui tenemos la hora actual

if [ $var1 == '1' ]; then ##encendemos el led verde
	encender
fi

if [ $var1 == '0' ]; then ##apaga el led verde
	apagar
fi

if [ $var1 == 'exit' ]; then ## salimos de bucle y borramos los gpios
	bash ./unexport.sh
	break
fi

if [ $var1 == '2' ]; then ##este sera para el tiempo y el gpio 22 sera el intermitente
	echo "Ponga la hora que desea finalizar: "
	read hora1
	echo "Iniciando contador.."
	encender
	while [ true ]; do
	actual2=`date +"%H:%M:%S"`
	parpadeo
	#echo $actual2
		if [ "$hora1" == "$actual2" ]; then
			apagar
			break
		fi
	done
fi

if [ $var1 == '3' ];then ## aqui iniciamos y apagamos el contador
	echo "Ponga la hora que desea iniciar: "
	read hora2
	echo "Ponga la hora que deseas finalizar:"
	read hora3

	while [ true ]; do
	actual3=`date +"%H:%M:%S"`
	parpadeo
		if [ "$actual3" == "$hora2" ]; then
			encender
		fi
		if [ "$actual3" == "$hora3" ]; then
			apagar
			break
		fi
	done
fi

if [ $var1 == 'help' ];then
	echo "0 - apagar"
	echo "1 - encender"
	echo "2 - iniciar contador para su apagado"
	echo "3 - iniciar y apagar con un contador"
	echo "exit - Salir del programa"
fi
done
echo "Adios"
echo 0 > /sys/class/gpio/gpio27/value
