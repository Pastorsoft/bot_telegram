#!/usr/bin/python

import telegram
import os
from telegram.ext import *
import logging

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s,  level=logging.INFO')

logger = logging.getLogger(__name__)


##############################

# Funcion para realizar llamadas del sistema
def llamadaSistema(entrada):
    salida = ""  # Creamos variable vacia
    f = os.popen(entrada)  # Llamada al sistema
    for i in f.readlines():  # Leemos caracter a caracter sobre la linea devuelta por la llamada al sistema
        salida += i  # Insertamos cada uno de los caracteres en nuestra variable
    salida = salida[:-1]  # Truncamos el caracter fin de linea '\n'

    return salida  # Devolvemos la respuesta al comando ejecutado

##############################


def start(bot, update):
    update.message.reply_text('Hola campeon')  # aqui solo mandamos un mensaje


def automatico(bot, update):
    # aqui recuperamos el id de quien esta mandando el mensaje
    chat_id = update.message.chat_id
    if (chat_id == 117222960):  # colocar numero de la id como int
        update.message.reply_text("No tienes Autorizacion")
    else:
        os.system('date')


def help(bot, update):
    listado = ["/start - inicia el bot", "/consultar - consulta pwd",
               "/automatico - para localizar cambios"]
    for l in listado:
        lista = l
        # Aqui pondre todos los comandos para ser visualizados
        update.message.reply_text(l)


def consultar(bot, update):
    # Aqui hacemos peticion para lectura de un archivo
    var1 = llamadaSistema('pwd')
    update.message.reply_text(var1)


def main():
    mi_bot = telegram.Bot(
        token="459472203:AAHClTMdjO-1A7LDfm_9APkJ7EsD51B05Uk")
    mi_bot_update = Updater(mi_bot.token)

    mi_bot_update.dispatcher.add_handler(CommandHandler('start', start))
    mi_bot_update.dispatcher.add_handler(CommandHandler('help', help))
    mi_bot_update.dispatcher.add_handler(
        CommandHandler('consultar', consultar))
    mi_bot_update.dispatcher.add_handler(
        CommandHandler('automatico', automatico))

    mi_bot_update.start_polling()
    mi_bot_update.idle()


if __name__ == "__main__":
    main()
